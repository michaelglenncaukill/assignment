﻿using Assignment;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Drawing;

namespace AssignmentTests
{
    [TestClass]
    public class CircleTests
    {
        /// <summary>
        /// set_S the state under test_ expected behavior.
        /// </summary>
        /// testing that the circle can set the value of radius
        [TestMethod]
        public void set_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            var circle = new Circle();
            Color colour = default(global::System.Drawing.Color);
            int[] list = { 0, 1, 2, 3 };

            // Act
            circle.set(colour, list);
            int radius = list[2];

            // Assert
            Assert.AreEqual(2, radius);
        }

        /// <summary>
        /// Tos the string_ state under test_ expected behavior.
        /// </summary>
        /// testing that the to string method actually outputs something
        [TestMethod]
        public void ToString_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            var circle = new Circle();

            // Act
            var result = circle.ToString();

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
