﻿using Assignment;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace AssignmentTests
{
    [TestClass]
    public class FactoryTests
    {
        [TestMethod]
        public void getShapeCircle_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            var factory = new Factory();
            String shapeType = "Circle";

            // Act
            var result = factory.getShape(shapeType);

            // Assert
            Assert.AreEqual("Circle", shapeType);
        }

        [TestMethod]
        public void getShapeRectangle_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            var factory = new Factory();
            String shapeType = "rectangle";

            // Act
            var result = factory.getShape(shapeType);

            // Assert
            Assert.AreEqual("rectangle", shapeType);
        }
    }
}
