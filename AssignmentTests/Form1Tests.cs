﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assignment;
using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment.Tests
{
    /// <summary>
    /// The form1 tests.
    /// </summary>
    [TestClass()]
    public class Form1Tests
    {
        /// <summary>
        /// Form1S the circle test.
        /// </summary>
        /// testing that circle can be added 
        [TestMethod()]
        public void Form1CircleTest()
        {
            // Arrange
            String shapeType = "Circle";
            var factory = new Factory();          

            // Act
            var result = factory.getShape(shapeType);

            // Assert
            Assert.AreEqual("Circle", shapeType);
        }

        /// <summary>
        /// Form1S the rectangle test.
        /// </summary>
        /// testing that rectangle can be added
        [TestMethod()]
        public void Form1RectangleTest()
        {
            // Arrange
            String shapeType = "Rectangle";
            var factory = new Factory();

            // Act
            var result = factory.getShape(shapeType);

            // Assert
            Assert.AreEqual("Rectangle", shapeType);
        }
    }
}