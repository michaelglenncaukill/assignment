﻿using Assignment;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Drawing;

namespace AssignmentTests
{
    [TestClass]
    public class RectangleTests
    {
        /// <summary>
        /// set_S the state under test_ expected behavior.
        /// </summary>
        /// testing that rectangle can set the value of x.
        [TestMethod]
        public void set_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            var rectangle = new Assignment.Rectangle();
            Color colour = default(global::System.Drawing.Color);
            int[] list = new int[] { 0, 0, 100, 100 };

            // Act
            rectangle.set(colour, list);
            int X = list[2];

            // Assert
            Assert.AreEqual(100, X);
        }

        /// <summary>
        /// set2_S the state under test_ expected behavior.
        /// </summary>
        /// testing that rectangle can set the colour
        [TestMethod]
        public void set2_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            var rectangle = new Assignment.Rectangle();
            Color colour = Color.Red;
            int[] list = new int[] { 0, 0, 100, 100 };

            // Act
            rectangle.set(colour, list);          

            // Assert
            Assert.AreEqual(Color.Red, colour);
        }

        /// <summary>
        /// Tos the string_ state under test_ expected behavior.
        /// </summary>
        /// testing that the to string method in rectangle has an output. 
        [TestMethod]
        public void ToString_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            var rectangle = new Assignment.Rectangle();

            // Act
            var result = rectangle.ToString();

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
