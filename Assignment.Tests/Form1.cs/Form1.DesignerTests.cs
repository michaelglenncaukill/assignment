namespace Assignment.Tests
{
    using Assignment;
    using System;
    using NUnit.Framework;
    using System.Threading;

    [TestFixture]
    [Apartment(ApartmentState.STA)]
    public class Form1Tests
    {
        private Form1 _testClass;

        [SetUp]
        public void SetUp()
        {
            _testClass = new Form1();
        }

        [Test]
        public void CanConstruct()
        {
            var instance = new Form1();
            Assert.That(instance, Is.Not.Null);
        }
    }
}