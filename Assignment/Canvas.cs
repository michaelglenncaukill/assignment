﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.ComponentModel;
using System.Windows.Forms;

namespace Assignment
{
    public class Canvas
    {      
        Graphics g;
        Brush Brush;
        Pen Pen;      
        public int xPos, yPos;       
        const int XSIZE = 408;
        const int YSIZE = 306;

        //initialises canvas to pen at 0,0
        /// <summary>
        /// Initializes a new instance of the <see cref="Canvas"/> class.
        /// </summary>
        /// <param name="g">The g.</param>
        public Canvas(Graphics g)
        {
            this.g = g;
            //XCanvasSize = XSIZE;
            //YCanvasSize = YSIZE;
            xPos = yPos = 0;
            Pen = new Pen(Color.Black, 1);
            Brush = new SolidBrush(Color.Black);
        }

        //Draws line from current position to specified coordiantes
        /// <summary>
        /// Draws the to.
        /// </summary>
        /// <param name="toX">The to x.</param>
        /// <param name="toY">The to y.</param>
        public void DrawTo(int toX, int toY)
        {
            if (toX < 0 || toX > XSIZE || toY < 0 || toY > YSIZE)
                MessageBox.Show("Invalid drawline parameters");
            g.DrawLine(Pen, xPos, yPos, toX, toY);//draw line 
            xPos = toX;
            yPos = toY;//updates pen pos as it has moved 
        }

        /// <summary>
        /// Moves the to.
        /// </summary>
        /// <param name="toX">The to x.</param>
        /// <param name="toY">The to y.</param>
        public void MoveTo(int toX, int toY)
        {
            xPos = toX;
            yPos = toY;
        }

        //Draws a rectangle at the current position with specified width and height 
        /// <summary>
        /// Rectangles the.
        /// </summary>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        public void Rectangle(int width, int height)
        {                      
            if (width < 0 || height < 0)
                MessageBox.Show("Invalid rectnagle parameters");
            g.DrawRectangle(Pen, xPos, yPos, width, height);//draw rectangle
            xPos = width;
            yPos = height;
        }

        /// <summary>
        /// Fills the rectangle.
        /// </summary>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        public void FillRectangle(int width, int height)
        {
            if (width < 0 || height < 0)
                MessageBox.Show("Invalid rectangle parameters");
            g.FillRectangle(Brush, xPos, yPos, width, height);
            xPos = width;
            yPos = height;
        }

        /// <summary>
        /// Fills the circle.
        /// </summary>
        /// <param name="radius">The radius.</param>
        public void FillCircle(int radius)
        {
            if (radius < 0)
                MessageBox.Show("Invalid circle parameters");
            g.FillEllipse(Brush, xPos, yPos, radius, radius);          
        }

        /// <summary>
        /// Resets the.
        /// </summary>
        public void Reset()
        {
            xPos = 0;
            yPos = 0;
        }

        //public void DrawTriangle(int width, int height)
        //{
        //if (width < 0 || height < 0)
        //   MessageBox.Show("Invalid Triangle Command");
        // Rectangle r = new Rectangle(xPos - (width / 2), yPos - (width / 2), width,  height);
        // int half = r.Width / 2;
        // g.DrawLine(Pen, r.X, r.Y + r.Height, r.X + r.Width, r.Y + r.Height);
        //g.DrawLine(Pen, r.X, r.Y + r.Height, r.X + half, r.Y);
        // g.DrawLine(Pen, r.X + r.Width, r.Y + r.Height, r.X + half, r.Y);

        // }

        /// <summary>
        /// Draws the circle.
        /// </summary>
        /// <param name="radius">The radius.</param>
        public void DrawCircle (int radius)
        {
            if (radius < 0)              
                 MessageBox.Show("Invalid radius value");
            g.DrawEllipse(Pen, xPos, yPos, radius, radius);                      
        }
      
        public void Red()
        {
            Pen.Color = Color.Red;            
        }
        public void Green()
        {
            Pen.Color = Color.Green;
        }
        public void Blue()
        {
            Pen.Color = Color.Blue;
        }
        public void Yellow()
        {
            Pen.Color = Color.Yellow;
        } 
        
        public void Clear()
        {
            g.Clear(Color.DarkGray);
        }
    
    }
}
