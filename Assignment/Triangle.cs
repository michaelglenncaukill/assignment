﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment
{
    class Triangle : Shape
    {
        int width, height;
        public Triangle() : base()
        {
            width = 100;
            height = 100;
        }

        public Triangle(Color colour, int x, int y) : base(colour, x, y)
        {
            this.width = width;
            this.height = height;
        }

        public override void set(Color colour, params int[] list)
        {
            base.set(colour, list[0], list[1]);
            this.width = list[2];
            this.height = list[3];
        }

        public override void draw(Graphics g)
        {
            Pen p = new Pen(Color.Black, 2);
            //Rectangle r = new Rectangle(xPos - (width / 2), yPos - (width / 2), width, height);
           // int half = r.Width / 2;
            //g.DrawLine(p, r.X, r.Y + r.Height, r.X + r.Width, r.Y + r.Height);
           // g.DrawLine(p, r.X, r.Y + r.Height, r.X + half, r.Y);
          //  g.DrawLine(p, r.X + r.Width, r.Y + r.Height, r.X + half, r.Y);
        }

        public override string ToString()
        {
            return base.ToString() + " " + this.x + this.y;
        }
    }
}
