﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Assignment
{
    public partial class Form1 : Form
    {
        const int BitmapX = 640;
        const int BitmapY = 480;
        //bitmap which will be displayed on picturebox
        Bitmap OutputBitmap = new Bitmap(BitmapX, BitmapY);
        Bitmap CursorBM = new Bitmap(BitmapX, BitmapY);
        Graphics bmG;
        Canvas MyCanvas;
        public bool Checked = false;
        ArrayList shapes = new ArrayList();//arraylist for the shapes
        public int xPos, yPos;
        Color PenColour = Color.Black;//pen colour 
        Pen P = new Pen(Color.Black, 1);//pen
        Boolean Flag = false;
        Thread newThread;

        /// <summary>
        /// Initializes a new instance of the <see cref="Form1"/> class.
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            newThread = new Thread(BasicThreadMethod);
            newThread.Start();
            Factory factory = new Factory();
            try
            {
                shapes.Add(factory.getShape("circle"));//adding circle to the factory
                shapes.Add(factory.getShape("rectangle"));//adding rectangle to the factory
            }
            catch(ArgumentException e)
            {
                Console.WriteLine("Invalid shape" + e);
            }                     
            bmG = Graphics.FromImage(OutputBitmap);
            MyCanvas = new Canvas(Graphics.FromImage(OutputBitmap));//class for handling the drawing
            xPos = yPos = 0;

        }

        /// <summary>
        /// Threads the method.
        /// </summary>
        public void BasicThreadMethod()
        {
            while(true)
            {
                if (Flag == false)
                {
                    P.Color = Color.Red;
                    Flag = true;
                } 
                else
                {
                    P.Color = Color.Green;
                    Flag = false;
                }
                Thread.Sleep(1000);

            }
        }


        /// <summary>
        /// Commands the line_ key down.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The e.</param>
        public void CommandLine_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)//when the enter key is pressed
            {
                //Parsing command line
                //Takes the value of the command line and first splits it by a space and removes any empty entries and assigns it to an array 
                //Assigns the command as the first value of the Input array
                // Assigns the parameters by splitting second value of Input by , and assigns them to a new array Parameters
                // if any of the paramaters are empty they are set to 0 to prevent any overflow
                String[] Input = CommandLine.Text.ToLower().Split(' ', (char)StringSplitOptions.RemoveEmptyEntries);
                var Command = Input[0];                
                int[] Parameters = Input.Length <= 1 ? new int[0] : Input[1].Split(',').Select(item => int.Parse(item)).ToArray();
                int X = Parameters.Length >= 1 ? Parameters[0] : 0;
                int Y = Parameters.Length >= 2 ? Parameters[1] : 0;
                Console.WriteLine(Command, X, Y);//printing the parameters to check they were seperated as expected           
                Commands(Command, X, Y);//calls commands and passes over the parameters                
                CommandLine.Text = "";//clear the command line
                Refresh(); // signify the display needs updating                                        
            }           

                void Commands(string Command, int X, int Y)//this is where all of the commands are executed. 
                {
                if (Command.Equals("drawto"))
                {
                    MyCanvas.DrawTo(X, Y);
                }
                else if (Command.Equals("moveto"))
                {
                    MyCanvas.MoveTo(X, Y);
                }
                else if (Command.Equals("rect"))
                {
                    if (Checked == true)
                    {
                        MyCanvas.FillRectangle(X, Y);
                    }
                    else
                    shapes.Add(new Rectangle(P.Color, 0, 0, X, Y));//creating a new rectangle using the paramters typed in the textbox
                }
                else if (Command.Equals("reset"))
                {
                    MyCanvas.Reset();
                }
                else if (Command.Equals("circle"))
                {
                    if (Checked == true)
                    {
                        MyCanvas.FillCircle(X);
                    }
                    else
                    shapes.Add(new Circle(P.Color, 0, 0, X));//creating a new circle using one parameter that will become radius in the factory
                }
                else if (Command.Equals("tri"))
                {
                   // MyCanvas.DrawTriangle(X, Y);
                }
                else if (Command.Equals("red"))
                {
                    this.PenColour = Color.Red;
                    MyCanvas.Red();                 
                }
                else if (Command.Equals("green"))
                {
                    this.PenColour = Color.Green;
                    MyCanvas.Green();
                }
                else if (Command.Equals("blue"))
                {
                    this.PenColour = Color.Blue;
                    MyCanvas.Blue();
                }
                else if (Command.Equals("yellow"))
                {
                    this.PenColour = Color.Yellow;
                    MyCanvas.Yellow();
                }
                else if (Command.Equals("clear"))
                {
                    MyCanvas.Clear();
                }
                else if (Command.Equals("redgreen"))
                {
                    ThreadMethod();
                }
                else
                {
                    MessageBox.Show("Invalid Command");
                }
            }
        }

        /// <summary>
        /// pictirebox1_paints the.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The e.</param>
        private void pictirebox1_paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.DrawImageUnscaled(OutputBitmap, 0, 0);//put the bitmap onto the output window
            for (int i = 0; i < shapes.Count; i++)
            {
                Shape s;
                s = (Shape)shapes[i];
                s.draw(g);
            }
        }

        /// <summary>
        ///  This is where the text in the program window is dealt with
        /// </summary>
        private void button1_Click(object sender, EventArgs e)//when the run button is clicked
        {
            string[] Lines = ProgramWindow.Text.Split('\n');
            int NumberOfCommands = Lines.Length;          
            {
                for (int i = 0; i <= NumberOfCommands - 1; i++)
                {
                    String[] Input = CommandLine.Text.ToLower().Split(' ', (char)StringSplitOptions.RemoveEmptyEntries);
                    String Command = Input[0];
                    int[] Parameters = Input.Length <= 1 ? new int[0] : Input[1].Split(',').Select(item => int.Parse(item)).ToArray();
                    int X = Parameters.Length >= 1 ? Parameters[0] : 0;
                    int Y = Parameters.Length >= 2 ? Parameters[1] : 0;
                    Console.WriteLine(Command, X, Y);                                                          
                    Commands(Command, X, Y);
                    Refresh();
                }
            }

            void Commands(string Command, int X, int Y)
            {              
                if (Command.Equals("drawto"))
                {
                   // MyCanvas.DrawTo(X, Y);
                }
                else if (Command.Equals("moveto"))
                {
                  //  MyCanvas.MoveTo(X, Y);
                }
                else if (Command.Equals("rect"))
                {
                    if (Checked == true)
                    {
                      //  MyCanvas.FillRectangle(X, Y);
                    }
                    else
                        shapes.Add(new Rectangle(Color.Black, 0, 0, X, Y));
                }
                else if (Command.Equals("reset"))
                {
                    MyCanvas.Reset();                                    
                }
                else if (Command.Equals("circle"))
                {
                    if (Checked == true)
                    {
                        //MyCanvas.FillCircle(X);
                    }                 
                    else
                        shapes.Add(new Circle(Color.Black, 0, 0, X));
                }
                else if (Command.Equals("tri"))
                {
                  //  MyCanvas.DrawTriangle(X, Y);
                }
                else if (Command.Equals("red"))
                {
                   // MyCanvas.Red();
                }
                else if (Command.Equals("green"))
                {
                    MyCanvas.Green();
                }
                else if (Command.Equals("blue"))
                {
                    MyCanvas.Blue();
                }
                else if (Command.Equals("yellow"))
                {
                    MyCanvas.Yellow();
                }
                else if (Command.Equals("clear"))
                {
                    MyCanvas.Clear();
                }              
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void OutputWindow_ControlRemoved(object sender, ControlEventArgs e)
        {

        }

        //saves the contents of the program windop to a text file
        /// <summary>
        /// Save_S the click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The e.</param>
        private void Save_Click(object sender, EventArgs e)
        {
            File.WriteAllText("D:\\ProgramWindow.txt", ProgramWindow.Text);
        }

        //loads the contents of the text file back to the program window
        /// <summary>
        /// Load_S the click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The e.</param>
        private void Load_Click(object sender, EventArgs e)
        {
            ProgramWindow.Text = File.ReadAllText("D:\\ProgramWindow.txt");
        }

        /// <summary>
        /// Clear_S the click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The e.</param>
        private void Clear_Click(object sender, EventArgs e)
        {
            MyCanvas.Clear();
        }

        /// <summary>
        /// Check_S the changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The e.</param>
        private void Check_Changed(object sender, EventArgs e)
        {
            Checked = true;                           
        }

    }
}
