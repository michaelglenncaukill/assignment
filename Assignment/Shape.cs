﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment
{
    public abstract class Shape:Shapes
    {

        protected Color colour; //shapes colour
        protected int x, y;

        /// <summary>
        /// Initializes a new instance of the <see cref="Shape"/> class.
        /// </summary>
        public Shape()
        {
            colour = Color.Black;
            x = y = 0;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Shape"/> class.
        /// </summary>
        /// <param name="colour">The colour.</param>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        public Shape(Color colour, int x, int y)
        {
            this.colour = colour;//shapes colour
            this.x = x; //shapes current x pos
            this.y = y; //shapes current y pos
        }

        /// <summary>
        /// draws the.
        /// </summary>
        /// <param name="g">The g.</param>
        public abstract void draw(Graphics g);

        /// <summary>
        /// sets the.
        /// </summary>
        /// <param name="colour">The colour.</param>
        /// <param name="list">The list.</param>
        public virtual void set(Color colour, params int[] list)
        {
            this.colour = colour;
            this.x = list[0];
            this.y = list[1] ;
        }

        /// <summary>
        /// Tos the string.
        /// </summary>
        /// <returns>A string.</returns>
        public override string ToString()
        {
            return base.ToString()+ "  " + this.x+ ","+ this.y + " : ";
        }
    }
}
