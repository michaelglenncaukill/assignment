﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment
{
    interface Shapes
    {
        /// <summary>
        /// sets the.
        /// </summary>
        /// <param name="c">The c.</param>
        /// <param name="list">The list.</param>
        void set(Color c, params int[] list);

        /// <summary>
        /// draws the.
        /// </summary>
        /// <param name="g">The g.</param>
        void draw(Graphics g);
    }
}
