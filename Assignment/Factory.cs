﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment
{
    public class Factory
    {
        /// <summary>
        /// gets the shape.
        /// </summary>
        /// <param name="shapeType">The shape type.</param>
        /// <returns>A Shape.</returns>
        public Shape getShape(String shapeType)
        {
            shapeType = shapeType.ToLower().Trim();

            if (shapeType.Equals("circle"))
            {
                return new Circle();
            }
            else if (shapeType.Equals("rectangle"))
            {
                return new Rectangle();
            }
            else
            {
                System.ArgumentException argEx = new System.ArgumentException("Factory error: " + shapeType + "does not exist");
                throw argEx;
            }

        }
    }
}
